//
//  ViewController.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-01-31.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit
import GoogleSignIn

protocol LoginVCDelegate: class {
    func loginVCDismissed()
}

class LoginViewController: UIViewController, GIDSignInDelegate {

    weak var delegate: LoginVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }

    // MARK: GIDSIGNIN
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error?) {
        if let error = error {
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        
            return
        }
        
        self.delegate?.loginVCDismissed()
        self.dismiss(animated: true)
    }
    
}

