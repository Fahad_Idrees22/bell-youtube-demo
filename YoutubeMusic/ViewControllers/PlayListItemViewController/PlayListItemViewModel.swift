//
//  PlayListItemViewModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-03.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListItemViewModel {

    private var playListModel: PlayListModel!
    private var playListItemModels: [PlayListItemModel] = []
    private var totalPlayListVideos = 0
    private var videoIds = ""
    
    var hasMore: Bool {
        if playListItemModels.count == totalPlayListVideos {
            return false
        }
        return true
    }
    
    init(playListModel: PlayListModel) {
        self.playListModel = playListModel
    }
    
    var playListTitle: String {
        return self.playListModel.title
    }
    
    var playListItemsCount: String {
        return String(self.playListModel.itemCount)
    }
    
    var playListThumbnailUrl: URL? {
        return URL(string: self.playListModel.thumbnailUrl)
    }
    
    func fetchPlayListItems(onSuccess:@escaping (() -> Void), onError:@escaping errorHandler) {
    
        FetchPlayListItemAPI.shared.makeFetchPlayListItemsRequest(playListId: self.playListModel.id, success: { (videoIds, count) in
            self.totalPlayListVideos = count
            FetchPlayListItemDetailsAPI.shared.makeFetchPlayListItemsDetailRequest(videoIds: self.getVideoIdsString(videoIds: videoIds), success: { (playListItemModels) in
                self.playListItemModels.append(contentsOf: playListItemModels)
                onSuccess()
            }) { (error) in
                onError(error)
            }
            
        }) { (error) in
            onError(error)
        }
    }
    
    private func getVideoIdsString(videoIds: [String]) -> String {
        return videoIds.joined(separator: ",")
    }
    
    func numberOfRows() -> Int {
        return playListItemModels.count
    }

    func cellViewModelForIndexPath(indexPath: IndexPath) -> PlayListItemViewCellViewModel {
        let playListItemModel = playListItemModels[indexPath.row]
        return PlayListItemViewCellViewModel(titleText: playListItemModel.title, author: playListItemModel.author, thumbnailUrl: playListItemModel.thumbnailUrl, duration: playListItemModel.duration)
    }
    
}
