//
//  PlayListItemViewCellViewModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-04.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListItemViewCellViewModel: NSObject {
    
    var titleText: String
    var author: String
    var thumbnailUrl: String
    var duration: String
    
    init(titleText: String, author: String, thumbnailUrl: String, duration: String) {
        self.titleText = titleText
        self.author = author
        self.thumbnailUrl = thumbnailUrl
        self.duration = duration.parseVideoDuration()
    }
    
    
    
}
