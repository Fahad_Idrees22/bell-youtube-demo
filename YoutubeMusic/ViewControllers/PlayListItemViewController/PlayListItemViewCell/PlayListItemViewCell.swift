//
//  PlayListItemViewCellTableViewCell.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-04.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListItemViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var duration: UILabel!
    
    func configure(indexPath: IndexPath, viewModel: PlayListItemViewCellViewModel) {
        self.title.text = viewModel.titleText
        self.author.text = viewModel.author
        self.duration.text = viewModel.duration
        
        if let url = URL(string: viewModel.thumbnailUrl) {
            self.thumbnailImage.load(url: url, placeholder: UIImage(named:"PlaceHolder"))
        }
    }
}
