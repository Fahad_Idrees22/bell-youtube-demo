//
//  PlayListDetailsViewController.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-01-31.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListItemViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var playListTitle: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var itemCount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var viewModel: PlayListItemViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.fetchPlayListItems()
    }
    
    // MARK: - Private methods
    private func setupUI() {
        self.playListTitle.text = viewModel.playListTitle
        self.itemCount.text = viewModel.playListItemsCount
        if let url = viewModel.playListThumbnailUrl {
            self.thumbnailImage.load(url: url, placeholder: UIImage(named:"PlaceHolder"))
        }
    }
    
    private func fetchPlayListItems() {
        self.view.activityStartAnimating(activityColor: .white, backgroundColor: UIColor.black.withAlphaComponent(0.1))
        viewModel.fetchPlayListItems(onSuccess: { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
            self.view.activityStopAnimating()
        }) { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.view.activityStopAnimating()
            }
        }
    }
    
    // MARK: - TableView Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListItemViewCellIdentifier", for: indexPath) as? PlayListItemViewCell else {
            return UITableViewCell()
        }
        
        let cellViewModel = viewModel.cellViewModelForIndexPath(indexPath: indexPath)
        cell.configure(indexPath: indexPath, viewModel: cellViewModel)
        
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (viewModel.numberOfRows() - 1), viewModel.hasMore {
            self.fetchPlayListItems()
        }
    }
    
}
