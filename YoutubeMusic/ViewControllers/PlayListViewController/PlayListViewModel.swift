//
//  PlayListViewModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-02.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation

class PlayListViewModel {

    private var playListModels: [PlayListModel] = []
    private var totalPlayLists = 0
    
    var hasMore: Bool {
        if playListModels.count == totalPlayLists {
            return false
        }
        return true
    }
    
    func fetchPlayList(onSuccess:@escaping (() -> Void), onError:@escaping errorHandler) {
    
        FetchPlayListAPI.shared.makeFetchPlayListRequest(success: { (playListModels, totalCount) in
            self.playListModels.append(contentsOf: playListModels)
            self.totalPlayLists = totalCount
            onSuccess()
        }) { (error) in
            onError(error)
        }
    }
    
    func numberOfRows() -> Int {
        return playListModels.count
    }
    
    func cellViewModelForIndexPath(indexPath: IndexPath) -> PlayListViewCellViewModel {
        let playListModel = playListModels[indexPath.row]
        return PlayListViewCellViewModel(titleText: playListModel.title, numberOfItems: String(playListModel.itemCount), thumbnailUrl: playListModel.thumbnailUrl)
    }
    
    func getPlayListModel(indexPath: IndexPath) -> PlayListModel {
        let playListModel = playListModels[indexPath.row]
        return playListModel
    }
}
