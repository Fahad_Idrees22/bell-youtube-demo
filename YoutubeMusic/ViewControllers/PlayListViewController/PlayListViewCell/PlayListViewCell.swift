//
//  PlayListViewCell.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-02.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var itemCount: UILabel!
    
    func configure(indexPath: IndexPath, viewModel: PlayListViewCellViewModel) {
        self.title.text = viewModel.titleText
        self.itemCount.text = viewModel.numberOfItems
        
        if let url = URL(string: viewModel.thumbnailUrl) {
            self.thumbnailImage.load(url: url, placeholder: UIImage(named:"PlaceHolder"))
        }
    }
}
