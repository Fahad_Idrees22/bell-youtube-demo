//
//  PlayListViewCellViewModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-02.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

class PlayListViewCellViewModel {

    var titleText: String
    var numberOfItems: String
    var thumbnailUrl: String
    
    init(titleText: String, numberOfItems: String, thumbnailUrl: String) {
        self.titleText = titleText
        self.numberOfItems = numberOfItems
        self.thumbnailUrl = thumbnailUrl
    }
}
