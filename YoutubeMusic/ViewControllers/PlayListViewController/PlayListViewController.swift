//
//  PlayListViewController.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-01-31.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit
import GoogleSignIn
import Alamofire

class PlayListViewController: UIViewController, GIDSignInDelegate, LoginVCDelegate, UITableViewDataSource, UITableViewDelegate {
    
    private var viewModel = PlayListViewModel()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/youtube")
        
        if let hasPreviousSignIn = GIDSignIn.sharedInstance()?.hasPreviousSignIn(), hasPreviousSignIn {
            GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        } else {
            self.showLoginViewController()
        }
    }
    
    // MARK: Private Functions
    private func showLoginViewController() {
        if let GIDSignIn = GIDSignIn.sharedInstance(), !GIDSignIn.hasPreviousSignIn(), let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            loginVC.delegate = self
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: false)
        }
    }
    
    private func fetchPlayList() {
        self.view.activityStartAnimating(activityColor: .white, backgroundColor: UIColor.black.withAlphaComponent(0.1))
        viewModel.fetchPlayList(onSuccess: { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
            self.view.activityStopAnimating()
        }) { [weak self] error in
            guard let self = self else { return }
            if let error = error {
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.view.activityStopAnimating()
            }
        }
    }
    
    // MARK: Login view controller delegate
    func loginVCDismissed() {
        self.fetchPlayList()
    }
    
    // MARK: - TableView Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlayListViewCellIdentifier", for: indexPath) as? PlayListViewCell else {
            return UITableViewCell()
        }
        
        let cellViewModel = viewModel.cellViewModelForIndexPath(indexPath: indexPath)
        cell.configure(indexPath: indexPath, viewModel: cellViewModel)
        
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (viewModel.numberOfRows() - 1), viewModel.hasMore {
            self.fetchPlayList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let playListModel = viewModel.getPlayListModel(indexPath: indexPath)
        self.performSegue(withIdentifier: "PlayListViewToPlayListItemView", sender: playListModel)
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayListViewToPlayListItemView", let playListModel = sender as? PlayListModel {
            let vc = segue.destination as! PlayListItemViewController
            let playListItemViewModel = PlayListItemViewModel(playListModel: playListModel)
            vc.viewModel = playListItemViewModel
        }
    }
    
    // MARK: - GIDSIGNIN
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        self.fetchPlayList()
    }
    
}
