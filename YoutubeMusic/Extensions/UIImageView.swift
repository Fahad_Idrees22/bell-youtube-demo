//
//  UIImageView.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-02.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

//Source: https://gist.github.com/amosavian/a05044e57c290b5e064f4f7acfc3b506

extension UIImageView {
    func load(url: URL, placeholder: UIImage?) {
        let cache = URLCache.shared
        let request = URLRequest(url: url)
        if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
            self.image = image
        } else {
            self.image = placeholder
            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                if let data = data, let response = response, (200..<300).contains(((response as? HTTPURLResponse)?.statusCode ?? 500)), let image = UIImage(data: data) {
                    let cachedData = CachedURLResponse(response: response, data: data)
                    cache.storeCachedResponse(cachedData, for: request)
                    DispatchQueue.main.async {
                        self.image = image
                    }
                }
            }).resume()
        }
    }
}
