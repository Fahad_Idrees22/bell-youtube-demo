//
//  APIClient.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-01.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation
import GoogleSignIn
import Alamofire

typealias completionHandler = (AnyObject?) -> Void
typealias errorHandler = (Error?) -> Void

protocol ApiProtocol: class {
    var params: Parameters? { get }
    var uri: String { get }
    var headers: HTTPHeaders { get }
    var method: HTTPMethod { get }
    var domain: String { get }
    var baseUrl: String { get }
    var ignoreSuccessResponse: Bool { get }
}

class APIClient {

    struct HttpHeaderKey {
        static var accept: String {return "Accept"}
        static var contentType: String {return "Content-Type"}
        static var authorization: String {return "Authorization"}
    }
    
    struct HttpParameterKey {
        static var part: String {return "part"}
        static var maxResults: String {return "maxResults"}
        static var mine: String {return "mine"}
        static var nextPageToken: String {return "pageToken"}
        static var playListId: String {return "playlistId"}
        static var videoId: String {return "id"}
    }
    
    
    var params: Parameters {
        fatalError("subclass need to override")
    }
    
    var headers: HTTPHeaders {
        let heads: HTTPHeaders = [
            HttpHeaderKey.authorization: self.authToken()
        ]

        return heads
    }
        
    var method: HTTPMethod {
        return .get
    }
    
    var uri: String {
        fatalError("subclass need to override")
    }
    
    var baseUrl: String {
        return "https://www.googleapis.com/youtube/v3/"
    }
    
    func makeRequest(success: @escaping completionHandler, failure: @escaping errorHandler) {
        
        Alamofire.request(self.url(baseUrl: baseUrl, uri: uri), method: method, parameters: self.params, headers: headers).responseJSON { response in
            
            switch response.result {
            case .success(let json):
                let statusCode = response.response?.statusCode ?? 500
                if (200..<300).contains(statusCode) {
                    self.onSuccess(json: json as AnyObject, successBlock: success)
                } else {
                    print(json)
                    failure(nil)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    func onSuccess(json: AnyObject?, successBlock: completionHandler) {
        fatalError("subclass need to override")
    }
    
    private func url(baseUrl: String, uri: String) -> String {
        return "\(baseUrl)\(uri)"
    }
    
    private func authToken() -> String {
        if let token = GIDSignIn.sharedInstance()?.currentUser.authentication.accessToken {
            return "Bearer " + token
        }
        return ""
    }
}
