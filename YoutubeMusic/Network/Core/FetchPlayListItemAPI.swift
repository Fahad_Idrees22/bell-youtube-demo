//
//  FetchPlayListItemAPI.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-03.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation
import Alamofire

class FetchPlayListItemAPI: APIClient {
    
    static let shared = FetchPlayListItemAPI()
    
    private var nextPageToken: String?
    private var playListId = ""
    typealias FetchPlayListItemsResponse = (totalPlayListItems: Int, videoIds: [String])
    
    override var params: Parameters {
        var params = [HttpParameterKey.part: "contentDetails", HttpParameterKey.maxResults: "10", HttpParameterKey.playListId: self.playListId]
        
        if nextPageToken != nil {
            params[HttpParameterKey.nextPageToken] = nextPageToken
        }
        
        return params
    }
    
    override var uri: String {
        return "playlistItems"
    }
    
    func makeFetchPlayListItemsRequest(playListId: String, success:@escaping (([String], Int) -> Void), failure:@escaping errorHandler) {
        self.playListId = playListId
        super.makeRequest(success: { responseObject  in
            if let fetchPlayListResponse = responseObject as? FetchPlayListItemsResponse {
                success(fetchPlayListResponse.videoIds, fetchPlayListResponse.totalPlayListItems)
            } else {
                failure(nil)
            }
        }) { (errorObject) in
            failure(errorObject)
        }
    }
    
    override func onSuccess(json: AnyObject?, successBlock: completionHandler) {
        if let jsonObject = json as? [String: Any] {
            
            if let nextPageTokenValue = jsonObject["nextPageToken"] as? String {
                nextPageToken = nextPageTokenValue
            } else {
                nextPageToken = nil
            }
            
            var totalCount = 0
            if let pageInfo = jsonObject["pageInfo"] as? [String: Any], let totalItems = pageInfo["totalResults"] as? Int {
                totalCount = totalItems
            }
            
            var videoIds : [String] = []
            if let items = jsonObject["items"] as? [AnyObject] {
                for item in items {
                    
                    if let contentDetails = item["contentDetails"] as? [String: Any], let videoId = contentDetails["videoId"] as? String {
                        videoIds.append(videoId)
                    }
                }
            }
            let response: FetchPlayListItemsResponse = (totalPlayListItems: totalCount, videoIds: videoIds)
            
            successBlock(response as AnyObject)
        }
    }
}
