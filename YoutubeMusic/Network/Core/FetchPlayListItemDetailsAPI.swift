//
//  FetchPlayListItemDetailsAPI.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-04.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation
import Alamofire

class FetchPlayListItemDetailsAPI: APIClient {
    
    static let shared = FetchPlayListItemDetailsAPI()
    private var videoIds = ""
    
    override var params: Parameters {
        let params = [HttpParameterKey.part: "snippet,contentDetails", HttpParameterKey.videoId: self.videoIds]
        
        return params
    }
    
    override var uri: String {
        return "videos"
    }
    
    func makeFetchPlayListItemsDetailRequest(videoIds: String, success:@escaping (([PlayListItemModel]) -> Void), failure:@escaping errorHandler) {
        self.videoIds = videoIds
        super.makeRequest(success: { responseObject  in
            if let playListItemModels = responseObject as? [PlayListItemModel] {
                success(playListItemModels)
            } else {
                failure(nil)
            }
        }) { (errorObject) in
            failure(errorObject)
        }
    }

    override func onSuccess(json: AnyObject?, successBlock: completionHandler) {
        if let jsonObject = json as? [String: Any] {

            var playListItemModels : [PlayListItemModel] = []
            if let items = jsonObject["items"] as? [AnyObject] {
                for item in items {
                    
                    if let contentDetails = item["contentDetails"] as? [String: Any], let snippet = item["snippet"] as? [String: Any] {
                        let title = (snippet["title"] as? String) ?? ""
                        let author = (snippet["channelTitle"] as? String) ?? ""
                        let duration = (contentDetails["duration"] as? String) ?? ""
                        var thumbnailUrl = ""
                        
                        if let thumbnails = snippet["thumbnails"] as? [String: Any], let medium = thumbnails["default"] as? [String: Any], let mediumUrl = medium["url"] as? String {
                            thumbnailUrl = mediumUrl
                        }
                        
                        let playListItemModel = PlayListItemModel(title: title, author: author, thumbnailUrl: thumbnailUrl, duration: duration)
                        playListItemModels.append(playListItemModel)
                    }
                }
            }

            successBlock(playListItemModels as AnyObject)
        }
    }
}
