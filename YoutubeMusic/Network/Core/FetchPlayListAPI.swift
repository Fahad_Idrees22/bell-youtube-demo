//
//  FetchPlayListAPI.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-01.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation
import Alamofire

class FetchPlayListAPI: APIClient {
    
    static let shared = FetchPlayListAPI()
    
    private var nextPageToken: String?
    typealias FetchPlayListResponse = (totalPlayLists: Int, playListModels: [PlayListModel])
    
    override var params: Parameters {
        var params = [HttpParameterKey.part: "snippet,contentDetails", HttpParameterKey.maxResults: "10", HttpParameterKey.mine: "true"]
        
        if nextPageToken != nil {
            params[HttpParameterKey.nextPageToken] = nextPageToken
        }
        
        return params
    }
    
    override var uri: String {
        return "playlists"
    }
    
    func makeFetchPlayListRequest(success:@escaping (([PlayListModel], Int) -> Void), failure:@escaping errorHandler) {
        super.makeRequest(success: { responseObject  in
            if let fetchPlayListResponse = responseObject as? FetchPlayListResponse {
                success(fetchPlayListResponse.playListModels, fetchPlayListResponse.totalPlayLists)
            } else {
                failure(nil)
            }
        }) { (errorObject) in
            failure(errorObject)
        }
    }
    
    override func onSuccess(json: AnyObject?, successBlock: completionHandler) {
        if let jsonObject = json as? [String: Any] {
            
            if let nextPageTokenValue = jsonObject["nextPageToken"] as? String {
                nextPageToken = nextPageTokenValue
            } else {
                nextPageToken = nil
            }
            
            var totalCount = 0
            if let pageInfo = jsonObject["pageInfo"] as? [String: Any], let totalItems = pageInfo["totalResults"] as? Int {
                totalCount = totalItems
            }
            
            if let items = jsonObject["items"] as? [AnyObject] {
                var playListModels : [PlayListModel] = []
                for item in items {
                    
                    if let playListId = item["id"] as? String, let contentDetails = item["contentDetails"] as? [String: Any], let snippet = item["snippet"] as? [String: Any] {
                        let itemCount = (contentDetails["itemCount"] as? Int) ?? 0
                        let title = (snippet["title"] as? String) ?? ""
                        var thumbnailUrl = ""
                        
                        if let thumbnails = snippet["thumbnails"] as? [String: Any], let medium = thumbnails["medium"] as? [String: Any], let mediumUrl = medium["url"] as? String {
                            thumbnailUrl = mediumUrl
                        }
                        
                        let playListModel = PlayListModel(id: playListId, title: title, itemCount: itemCount, thumbnailUrl: thumbnailUrl)
                        playListModels.append(playListModel)
                    }
                }
                let response: FetchPlayListResponse = (totalPlayLists: totalCount, playListModels: playListModels)
                
                successBlock(response as AnyObject)
            }
        }
    }
}
