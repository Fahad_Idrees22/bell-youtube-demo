//
//  PlayListModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-01.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import Foundation

struct PlayListModel {

    var id: String
    var title: String
    var itemCount: Int
    var thumbnailUrl: String
}
