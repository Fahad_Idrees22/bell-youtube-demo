//
//  PlayListItemModel.swift
//  YoutubeMusic
//
//  Created by Fahad Idrees on 2020-02-03.
//  Copyright © 2020 Fahad Idrees. All rights reserved.
//

import UIKit

struct PlayListItemModel {
    
    var title: String
    var author: String
    var thumbnailUrl: String
    var duration: String
}
